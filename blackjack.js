const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {

  constructor(name) {
    this.name = name;
    this.hand = [];
    this.drawCard = function (){
      var randomNumber = Math.ceil(Math.random() * 52) 
      const card = blackjackDeck[randomNumber];
      this.hand.push(card);
    };
  }

}; //TODO

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer("dealer"); // TODO
const player = new CardPlayer("krishnan"); // TODO

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  var blackJackScore = {total: 0, isSoft:false}
  // CREATE FUNCTION HERE
  for(i=0;i<hand.length;i++)
  {
    var card = hand[i];
    if(card.displayVal == "Ace") {
      blackJackScore.isSoft = true;
    }
    if(blackJackScore.isSoft) {
      blackJackScore.total += 1;
    }
    blackJackScore.total += card.val;
  }
  return blackJackScore;
}

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {

const result = calcPoints(dealerHand);
if(result.total < 16)
  return true;
else if(result.total == 17 && result.isSoft == true)
  return true;
else if(result.total > 17)
  return false;
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
  const winner ="";
  if(playerScore > dealerScore) {
    winner = player.name;
  }
  else if(playerScore < dealerScore) {
    winner = dealer.name;
  }
  else {
    winner = "Tie";
  }
  console.log(`PlayerScore: ${playerScore}, DealerScore: ${dealerScore}, Wins: ${winner}`)
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

const displayHandInHTML = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  const dealerLink = document.getElementById('dealerhand');
  const playerLink = document.getElementById('playerhand');
  if(player.name == "dealer")
    dealerLink.innerText = `Dealer's Hand is ${displayHand.join(', ')}`;
  else
  playerLink.innerText = `Player's Hand is ${displayHand.join(', ')}`;
}
/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();
if(calcPoints(player.hand).total == 21)
  return determineWinner(1,0);
else if(calcPoints(dealer.hand).total == 21)
  return determineWinner(0,1);
  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
  return displayHandInHTML(dealer);
  return displayHandInHTML(player);
}
 console.log(startGame());